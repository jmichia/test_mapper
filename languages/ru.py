# coding: utf8
{
'!langcode!': 'ru',
'!langname!': 'Русский',
'"update" is an optional expression like "field1=\'newvalue\'". You cannot update or delete the results of a JOIN': '"Изменить" - необязательное выражение вида "field1=\'новое значение\'". Результаты операции JOIN нельзя изменить или удалить.',
'%d days ago': '%d %%{день} тому',
'%d hours ago': '%d %%{час} тому',
'%d minutes ago': '%d %%{минуту} тому',
'%d months ago': '%d %%{месяц} тому',
'%d seconds ago': '%d %%{секунду} тому',
'%d weeks ago': '%d %%{неделю} тому',
'%d years ago': '%d %%{год} тому',
'%s %%{row} deleted': '%%{!удалена[0]} %s %%{строка[0]}',
'%s %%{row} updated': '%%{!изменена[0]} %s %%{строка[0]}',
'%s selected': '%%{!выбрана[0]} %s %%{запись[0]}',
'%Y-%m-%d': '%Y-%m-%d',
'%Y-%m-%d %H:%M:%S': '%Y-%m-%d %H:%M:%S',
'---Básicos---': '---Básicos---',
'---GIS---': '---GIS---',
'---Nivel 1---': '---Nivel 1---',
'---Nivel 2---': '---Nivel 2---',
'---Nivel 3---': '---Nivel 3---',
'1 day ago': '1 день тому',
'1 hour ago': '1 час тому',
'1 minute ago': '1 минуту тому',
'1 month ago': '1 месяц тому',
'1 second ago': '1 секунду тому',
'1 week ago': '1 неделю тому',
'1 year ago': '1 год тому',
'About': 'About',
'Access Control': 'Access Control',
'Actividad seleccionada': 'Actividad seleccionada',
'Activities': 'Activities',
'Activities Accepted': 'Activities Accepted',
'Activity': 'Activity',
'Administration': 'Administration',
'Administrative interface': 'административный интерфейс',
'Administrative Interface': 'Administrative Interface',
'Ajax Recipes': 'Ajax Recipes',
'Answer': 'Answer',
'Answer Type Accepted': 'Answer Type Accepted',
'Answer Types': 'Answer Types',
'appadmin is disabled because insecure channel': 'appadmin is disabled because insecure channel',
'Archivo adjunto': 'Archivo adjunto',
'Archivo analizado. Número de registros procesados: %s': 'Archivo analizado. Número de registros procesados: %s',
'Archivo de datos': 'Archivo de datos',
'Archivo origen': 'Archivo origen',
'Archivo sin registros procesables o con problemas.': 'Archivo sin registros procesables o con problemas.',
'Are you sure you want to delete this object?': 'Вы уверены, что хотите удалить этот объект?',
'Area': 'Area',
'Area/Zona de ubicacion': 'Area/Zona de ubicacion',
'Areas': 'Areas',
'Areas Accepted': 'Areas Accepted',
'Available Databases and Tables': 'Базы данных и таблицы',
'Buy this book': 'Buy this book',
'Cache': 'Cache',
'cache': 'cache',
'Cache Keys': 'Cache Keys',
'Cannot be empty': 'Пустое значение недопустимо',
'Change Password': 'Смените пароль',
'Check to delete': 'Удалить',
'Check to delete:': 'Удалить:',
'Choice': 'Choice',
'Choices': 'Choices',
'Choices Accepted': 'Choices Accepted',
'Clear CACHE?': 'Clear CACHE?',
'Clear DISK': 'Clear DISK',
'Clear RAM': 'Clear RAM',
'Client IP': 'Client IP',
'Color (#00FF00)': 'Color (#00FF00)',
'Color Fill': 'Color Fill',
'Color Line': 'Color Line',
'Comentarios': 'Comentarios',
'Comment': 'Comment',
'Community': 'Community',
'Components and Plugins': 'Components and Plugins',
'Configuration': 'Configuration',
'Content': 'Content',
'Content Data': 'Content Data',
'Contents Accepted': 'Contents Accepted',
'Controller': 'Controller',
'Coordenadas(Puntos)': 'Coordenadas(Puntos)',
'Coordenadas(Puntos) del lugar de estudio': 'Coordenadas(Puntos) del lugar de estudio',
'Coordinates': 'Coordinates',
'Copyright': 'Copyright',
'Current request': 'Текущий запрос',
'Current response': 'Текущий ответ',
'Current session': 'Текущая сессия',
'customize me!': 'настройте внешний вид!',
'Data Type': 'Data Type',
'Data Type Accepted': 'Data Type Accepted',
'Data Types': 'Data Types',
'data uploaded': 'данные загружены',
'Database': 'Database',
'Database %s select': 'выбор базы данных %s',
'db': 'БД',
'DB Model': 'DB Model',
'Define Layers': 'Define Layers',
'Delete:': 'Удалить:',
'Demo': 'Demo',
'Denominacion de la opción': 'Denominacion de la opción',
'Denominacion del adjunto': 'Denominacion del adjunto',
'Denominacion del area/zona': 'Denominacion del area/zona',
'Denominacion del lugar/pueblo/centro poblado': 'Denominacion del lugar/pueblo/centro poblado',
'Denominacion del medio ambiente': 'Denominacion del medio ambiente',
'Denominacion del periodo': 'Denominacion del periodo',
'Denominacion del proyecto': 'Denominacion del proyecto',
'Denominacion del Topico': 'Denominacion del Topico',
'Denominación del estudio': 'Denominación del estudio',
'Denominación del Estudio': 'Denominación del Estudio',
'Denominación del TAG': 'Denominación del TAG',
'Dependence': 'Dependence',
'Dependencia con otra areas (autocompletado)': 'Dependencia con otra areas (autocompletado)',
'Deployment Recipes': 'Deployment Recipes',
'Desarrollo de la actividad (Respuesta)': 'Desarrollo de la actividad (Respuesta)',
'Descripcion del medio ambiente': 'Descripcion del medio ambiente',
'Descripcion del periodo': 'Descripcion del periodo',
'Description': 'Описание',
'design': 'дизайн',
'Designed structure': 'Designed structure',
'Detailed Information': 'Detailed Information',
'Diseño de Popup': 'Diseño de Popup',
'DISK': 'DISK',
'Disk Cache Keys': 'Disk Cache Keys',
'Disk Cleared': 'Disk Cleared',
'Doc': 'Doc',
'Documentation': 'Documentation',
"Don't know what to do?": "Don't know what to do?",
'done!': 'готово!',
'Download': 'Download',
'E-mail': 'E-mail',
'Edit current record': 'Редактировать текущую запись',
'Edit Profile': 'Редактировать профиль',
'Email and SMS': 'Email and SMS',
'End Date': 'End Date',
'enter an integer between %(min)g and %(max)g': 'enter an integer between %(min)g and %(max)g',
'Environment': 'Environment',
'Environment Accepted': 'Environment Accepted',
'Environments': 'Environments',
'Error. Revise las actividades!': 'Error. Revise las actividades!',
'ERROR: Inconsistency': 'ERROR: Inconsistency',
'Errors': 'Errors',
'Estado': 'Estado',
'export as csv file': 'экспорт в  csv-файл',
'FAQ': 'FAQ',
'Fecha de culminacion del periodo': 'Fecha de culminacion del periodo',
'Fecha de inicio del periodo': 'Fecha de inicio del periodo',
'File Data': 'File Data',
'First name': 'Имя',
'Forms and Validators': 'Forms and Validators',
'Free Applications': 'Free Applications',
'GIS': 'GIS',
'GIS Layers': 'GIS Layers',
'Graph Model': 'Graph Model',
'Group': 'Group',
'Group ID': 'Group ID',
'Groups': 'Groups',
'Groups Accepted': 'Groups Accepted',
'Grupo objeto de estudio': 'Grupo objeto de estudio',
'Header Accepted': 'Header Accepted',
'Hello World': 'Заработало!',
'Home': 'Home',
'How did you get here?': 'How did you get here?',
'Image': 'Image',
'import': 'import',
'Import/Export': 'Импорт/экспорт',
'Index': 'Index',
'Individuals': 'Individuals',
'Information': 'Information',
'Ingresar archivo pre-formateado': 'Ingresar archivo pre-formateado',
'Ingrese el nombre del lugar': 'Ingrese el nombre del lugar',
'Ingrese el nombre del proyecto': 'Ingrese el nombre del proyecto',
'Ingrese el nombre del topico': 'Ingrese el nombre del topico',
'Input User': 'Input User',
'insert new': 'добавить',
'insert new %s': 'добавить %s',
'Internal State': 'Внутренне состояние',
'Introduction': 'Introduction',
'Invalid email': 'Неверный email',
'Invalid login': 'Неверный логин',
'Invalid password': 'Неверный пароль',
'Invalid Query': 'Неверный запрос',
'invalid request': 'неверный запрос',
'Key': 'Key',
'Last name': 'Фамилия',
'Layer Accepted': 'Layer Accepted',
'Layout': 'Layout',
'Layout Plugins': 'Layout Plugins',
'Layouts': 'Layouts',
'Live Chat': 'Live Chat',
'Logged in': 'Вход выполнен',
'Logged out': 'Выход выполнен',
'Login': 'Вход',
'login': 'вход',
'logout': 'выход',
'Logout': 'Выход',
'Lost Password': 'Забыли пароль?',
'Lost password?': 'Lost password?',
'Lugar de estudio': 'Lugar de estudio',
'Manage Cache': 'Manage Cache',
'Map': 'Map',
'Maps': 'Maps',
'Masive input': 'Masive input',
'Menu Model': 'Menu Model',
'Mutiple-Multiple': 'Mutiple-Multiple',
'Mutiple-One': 'Mutiple-One',
'Mutiple-Order': 'Mutiple-Order',
'My Sites': 'My Sites',
'Name': 'Name',
'New password': 'Новый пароль',
'New Record': 'Новая запись',
'new record inserted': 'новая запись добавлена',
'next 100 rows': 'следующие 100 строк',
'No databases in this application': 'В приложении нет баз данных',
'Nombre de la capa': 'Nombre de la capa',
'now': 'сейчас',
'Object or table name': 'Object or table name',
'Old password': 'Старый пароль',
'Online examples': 'примеры он-лайн',
'Opciones, separadas por barra en orden deseado': 'Opciones, separadas por barra en orden deseado',
'Options': 'Options',
'or import from csv file': 'или импорт из csv-файла',
'Orden de visualizacion': 'Orden de visualizacion',
'Order': 'Order',
'Origin': 'Происхождение',
'Other Plugins': 'Other Plugins',
'Other Recipes': 'Other Recipes',
'Overview': 'Overview',
'password': 'пароль',
'Password': 'Пароль',
"Password fields don't match": 'Пароли не совпадают',
'People Accepted': 'People Accepted',
'Period': 'Period',
'Period Accepted': 'Period Accepted',
'Periodo del estudio': 'Periodo del estudio',
'Periods': 'Periods',
'Person': 'Person',
'Place': 'Place',
'Place Accepted': 'Place Accepted',
'Place Definition': 'Place Definition',
'Place Name': 'Place Name',
'Places': 'Places',
'Places Accepted': 'Places Accepted',
'Plugins': 'Plugins',
'Popup Design': 'Popup Design',
'Posicion': 'Posicion',
'Powered by': 'Powered by',
'Preface': 'Preface',
'Pregunta a desarrollar': 'Pregunta a desarrollar',
'Presentación de Datos': 'Presentación de Datos',
'previous 100 rows': 'предыдущие 100 строк',
'Priority': 'Priority',
'profile': 'профиль',
'Project': 'Project',
'Project Input': 'Project Input',
'Project Name': 'Project Name',
'Project Tree': 'Project Tree',
'Projects': 'Projects',
'Projects Accepted': 'Projects Accepted',
'Proyecto desarrollado': 'Proyecto desarrollado',
'Puntaje, separados por barra en orden según opción': 'Puntaje, separados por barra en orden según opción',
'pygraphviz library not found': 'pygraphviz library not found',
'Python': 'Python',
'Query:': 'Запрос:',
'Question': 'Question',
'Quick Examples': 'Quick Examples',
'RAM': 'RAM',
'RAM Cache Keys': 'RAM Cache Keys',
'Ram Cleared': 'Ram Cleared',
'Recipes': 'Recipes',
'Record': 'Record',
'record does not exist': 'запись не найдена',
'Record ID': 'ID записи',
'Record id': 'id записи',
'Reference': 'Reference',
'Reference ID': 'Reference ID',
'Reference Table': 'Reference Table',
'Register': 'Зарегистрироваться',
'Registration identifier': 'Registration identifier',
'Registration key': 'Ключ регистрации',
'Reglas de Datos': 'Reglas de Datos',
'Reglas de Datos=titulo,condición,propiedad,valor,estilo|': 'Reglas de Datos=titulo,condición,propiedad,valor,estilo|',
'Relation Between Place/Name already exists!': 'Relation Between Place/Name already exists!',
'Remember me (for 30 days)': 'Запомнить меня (на 30 дней)',
'Reports': 'Reports',
'Reset Password key': 'Сбросить ключ пароля',
'Role': 'Роль',
'Row Accepted': 'Row Accepted',
'Rows in Table': 'Строк в таблице',
'Rows selected': 'Выделено строк',
'Rule Data': 'Rule Data',
'Save model as...': 'Save model as...',
'Score': 'Score',
'Scores': 'Scores',
'Search Place': 'Search Place',
'Seleccione el proyecto': 'Seleccione el proyecto',
'Seleccione el topico': 'Seleccione el topico',
'Seleccione la actividad': 'Seleccione la actividad',
'Seleccione la(s) opcion(es)': 'Seleccione la(s) opcion(es)',
'Semantic': 'Semantic',
'Services': 'Services',
'Size of cache:': 'Size of cache:',
'Spreadsheet File': 'Spreadsheet File',
'Start Date': 'Start Date',
'state': 'состояние',
'Statistics': 'Statistics',
'Status': 'Status',
'Studies': 'Studies',
'Style Data': 'Style Data',
'Stylesheet': 'Stylesheet',
'Submit': 'Отправить',
'submit': 'submit',
'Sujeto de estudio': 'Sujeto de estudio',
'Support': 'Support',
'Sure you want to delete this object?': 'Подтвердите удаление объекта',
'Table': 'таблица',
'Table name': 'Имя таблицы',
'Tag Accepted': 'Tag Accepted',
'Tags': 'Tags',
'Tags / Palabras clave': 'Tags / Palabras clave',
'Text': 'Text',
'The "query" is a condition like "db.table1.field1==\'value\'". Something like "db.table1.field1==db.table2.field2" results in a SQL JOIN.': '"Запрос" - это условие вида "db.table1.field1==\'значение\'". Выражение вида "db.table1.field1==db.table2.field2" формирует SQL JOIN.',
'The Core': 'The Core',
'The output of the file is a dictionary that was rendered by the view %s': 'The output of the file is a dictionary that was rendered by the view %s',
'The Views': 'The Views',
'This App': 'This App',
'Time': 'Time',
'Time in Cache (h:m:s)': 'Time in Cache (h:m:s)',
'Timestamp': 'Отметка времени',
'Tipo de dato': 'Tipo de dato',
'Tipo de dato contenido': 'Tipo de dato contenido',
'Tipo de medio ambiente': 'Tipo de medio ambiente',
'Tipo de medio ambiente (autocompletado)': 'Tipo de medio ambiente (autocompletado)',
'Tipo de respuesta': 'Tipo de respuesta',
'Topic': 'Topic',
'Topic Name': 'Topic Name',
'Topico de nivel superior': 'Topico de nivel superior',
'Topics': 'Topics',
'Traceback': 'Traceback',
'Twitter': 'Twitter',
'Type': 'Type',
'unable to parse csv file': 'нечитаемый csv-файл',
'Update:': 'Изменить:',
'Use (...)&(...) for AND, (...)|(...) for OR, and ~(...)  for NOT to build more complex queries.': 'Для построение сложных запросов используйте операторы "И": (...)&(...), "ИЛИ": (...)|(...), "НЕ": ~(...).',
'User %(id)s Logged-in': 'Пользователь %(id)s вошёл',
'User %(id)s Logged-out': 'Пользователь %(id)s вышел',
'User %(id)s Password changed': 'Пользователь %(id)s сменил пароль',
'User %(id)s Profile updated': 'Пользователь %(id)s обновил профиль',
'User %(id)s Registered': 'Пользователь %(id)s зарегистрировался',
'User ID': 'ID пользователя',
'Users Groups': 'Users Groups',
'Users Manage': 'Users Manage',
'Users Membership': 'Users Membership',
'Valor Asignado': 'Valor Asignado',
'Verify Password': 'Повторите пароль',
'Videos': 'Videos',
'View': 'View',
'Welcome': 'Welcome',
'Welcome to web2py': 'Добро пожаловать в web2py',
'Welcome to web2py!': 'Welcome to web2py!',
'Which called the function %s located in the file %s': 'Which called the function %s located in the file %s',
'You are successfully running web2py': 'You are successfully running web2py',
'You can modify this application and adapt it to your needs': 'You can modify this application and adapt it to your needs',
'You visited the url %s': 'You visited the url %s',
}
