Social Mapper
-------------


Description
***********

It is an application that allows the management of social studies information and is specifically targeted to communities or groups, for example, from surveys to ethnographies. Your application can be in various specialties, such as: social research, communication, anthropology, sociology, etc..

Includes support for GIS, so you can integrate your research projects and put them in the space.


Features
********

- Web browser based

- Multiple project support

- Easy configuration (to support any project)

- Group / Person studies

- Attachment support

- Openlayers integration for GIS (through GeoJSON)

- Massive input through pre-formated excel files: project tree and answers

- Full user manage and authorization

- A lot more....


Considerations
**************

- Social Mapper manages the information, however doesn't offer support for advanced reporting... We use *Pentaho* for basic and advanced reporting and also analytics.  Very soon we share some examples about how to do it.

- The social investigation is complex because lacks of metrics (applied maths) to compare the results: you have to score the answers! Is the only way, but you must analyze how to do it!


Requirements
************

- Web2py (latest version)

- Postgresql (v9 or better)

- Openlayers (included)


Installation
************

- Go to the applications directory inside *web2py* and clone (or pull) this repo

- Change the database information inside models/db.py


License
*******

Under GPL v3 , read the LICENSE file for details


Author
******

Alfonso de la Guarda Reyes <alfonsodg@gmail.com>


Funding
*******

CIMA ONG <http://www.cima.org.pe/>
