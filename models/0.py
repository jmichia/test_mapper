from gluon.storage import Storage
settings = Storage()

settings.migrate = True
settings.title = 'Social Mapper'
settings.subtitle = 'powered by web2py'
settings.author = 'Alfonso de la Guarda Reyes'
settings.author_email = 'alfonsodg@gmail.com'
settings.keywords = ''
settings.description = ''
settings.layout_theme = 'Default'
settings.database_uri = 'sqlite://storage.sqlite'
settings.security_key = '594ffb3a-3edc-42c1-961e-0d6a814f3a9b'
settings.email_server = 'localhost'
settings.email_sender = 'alfonsodg@gmail.com'
settings.email_login = ''
settings.login_method = 'local'
settings.login_config = ''
settings.plugins = []
